﻿using Activity2Part1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Activity2Part1.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        public ActionResult Index()
        {
            List<UserModel> userModels = new List<UserModel>();
            UserModel user1 = new UserModel("William", "abc@abc.com", "1010230");
            UserModel user2 = new UserModel("Jeff", "ac@abc.com", "1030230");
            userModels.Add(user1);
            userModels.Add(user2);
            return View("Test", userModels);
        }
    }
}